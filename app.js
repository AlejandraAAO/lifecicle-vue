//al crear una instancia  beforeCreate new Vue
const app = new Vue({

    el:'#app',
    data:{
        sayHello:'ciclo de vida con vue'

    },
    beforeCreate(){
        //esto se ejecuta antes o en el moemento q se crea vue
        console.log('beforeCreate');
    },
    created(){
        //cuando lee los metodos observadores eventos pero no accede al dom
        //no accede a  el = osea el elemento html enlazado
        console.log('created');
    },
    beforeMount(){
        //antes de q se inserte o modificque el DOM
        console.log('beforeMount');
    },
    mounted(){
        //al reemplazar la info en el html desde  .vue o js
        console.log('mounted');
    },
    beforeUpdate(){
        //adicional: al detectar cambios en el html
        console.log('beforeUpdate');
    },
    updated(){
        //adi: al efectuar los cambios
        console.log('updated');
    },
    beforeDestroy(){
        //antes de destruir la instancia
        console.log('beforeDestroy');
    },
    destroyed(){
        //instancia muerta
        console.log('destroyed')
    },
    methods:{
        dele(){
            this.$destroy();
        }
    }

})